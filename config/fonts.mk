# Copyright (C) 2021 KudProject Development
#
# SPDX-License-Identifier: Apache-2.0

VENDOR_PATH := vendor/extra

# Soong namespace
PRODUCT_SOONG_NAMESPACES += \
    $(VENDOR_PATH)/fonts

# Commissioner font
PRODUCT_PACKAGES += \
    Roboto-BlackItalic.ttf \
    Roboto-Black.ttf \
    Roboto-BoldItalic.ttf \
    Roboto-Bold.ttf \
    Roboto-ExtraBoldItalic.ttf \
    Roboto-ExtraBold.ttf \
    Roboto-ExtraLightItalic.ttf \
    Roboto-ExtraLight.ttf \
    Roboto-Italic.ttf \
    Roboto-LightItalic.ttf \
    Roboto-Light.ttf \
    Roboto-MediumItalic.ttf \
    Roboto-Medium.ttf \
    Roboto-Regular.ttf \
    Roboto-SemiBoldItalic.ttf \
    Roboto-SemiBold.ttf \
    Roboto-ThinItalic.ttf \
    Roboto-Thin.ttf

# Alegreya Sans font
PRODUCT_PACKAGES += \
    RobotoCondensed-BlackItalic.ttf \
    RobotoCondensed-Black.ttf \
    RobotoCondensed-BoldItalic.ttf \
    RobotoCondensed-Bold.ttf \
    RobotoCondensed-ExtraBoldItalic.ttf \
    RobotoCondensed-ExtraBold.ttf \
    RobotoCondensed-Italic.ttf \
    RobotoCondensed-LightItalic.ttf \
    RobotoCondensed-Light.ttf \
    RobotoCondensed-MediumItalic.ttf \
    RobotoCondensed-Medium.ttf \
    RobotoCondensed-Regular.ttf \
    RobotoCondensed-ThinItalic.ttf \
    RobotoCondensed-Thin.ttf
